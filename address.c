// 
//  address.c
//  pybitcoin-crypto
// 
//  Copyright 2013, 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of unsigned charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#include "address.h"
#include "base58powers.h"
#include "crypto.h"

bool AddrDataIsMoreThan58(unsigned char * addrData, int datalen) {

    for (int x = 0; x < datalen - 1; x++)
        if (addrData[x] != 0)
            return true;

    return addrData[datalen - 1] > 58;

}

int AddrDataMod58(unsigned char * addrData, int datalen){

    // Use method presented here: http://stackoverflow.com/a/10441333/238411

    // Prevent overflow in calculations
    int result = 0; 

    for(int x = 0; x < datalen; x++) {
        result *= (256 % 58);
        result %= 58;
        result += addrData[x] % 58;
        result %= 58;
    }

    return result;

}

void AddrDataSubtractByInt8(unsigned char * addrData, int datalen, int carry) {

    int x = datalen - 1;

    for (; addrData[x] < carry; x--){
        addrData[x] = 255 - carry + addrData[x] + 1;
        carry = 1;
    }

    addrData[x] -= carry;

}

void AddrDataDiv58(unsigned char * addrData, int datalen) {

    bool zero = true;
    for (int x = 0; x < datalen; x++) 
        if (addrData[x] != 0) {
            zero = false;
            break;
        }

    if (zero)
        return;

    unsigned char ans[datalen];
    int temp = 0;

    // base-256 long division.
    for (int x = 0; x < datalen; x++) {
        temp <<= 8;
        temp |= addrData[x];
        ans[x] = temp / 58;
        temp -= ans[x] * 58;
    }

    memmove(addrData, ans, datalen);

}


void DataToBase58Addr(unsigned char * data, int datalen, char * addr, int prefix) {

    // 1 prefix byte, data, 4 checksum bytes, plus extra 28 bytes of hash to avoid needless copying
    unsigned char addrData[1 + datalen + 32]; 

    addrData[0] = prefix;
    memcpy(addrData + 1, data, datalen);

    Sha256d(addrData, datalen + 1, addrData + datalen + 1);

    // Add prefix and checksum components to data length
    datalen += 5;

    // Encode to base58, seeing the bytes as big-endian

    int x = 0;

    // Zeros

    for (int y = 0; y < datalen; y++)
        if (! addrData[y]) {
            addr[x] = '1';
            x++;
        } else
            break;

    int zeros = x;

    // Encode

    for (; AddrDataIsMoreThan58(addrData, datalen); x++) {
        int mod = AddrDataMod58(addrData, datalen);
        addr[x] = BASE_58_CHARS[mod];
        AddrDataSubtractByInt8(addrData, datalen, mod);
        AddrDataDiv58(addrData, datalen);
    }

    addr[x++] = BASE_58_CHARS[addrData[datalen - 1]];

    // Reversal

    for (int y = 0; y < (x-zeros) / 2; y++) {
        char temp = addr[y+zeros];
        addr[y+zeros] = addr[x-y-1];
        addr[x-y-1] = temp;
    }

    addr[x] = '\0';

}

void AddrDataMultByUInt8(unsigned char * n, char i) {

    if (i == 0) {
        memset(n, 0, 25);
        return;
    }

    if (memcmp(n, (unsigned char[25]){0}, 25) == 0)
        return;

    // Multiply b by each byte and then add to answer

    int carry = 0;

    for (int x = 0; x < 25; x++) {
        // Allow for overflow onto next byte.
        carry = carry + n[x] * i; 
        n[x] = carry;
        carry >>= 8;
    }

}

void AddrDataAdd(unsigned char * a, unsigned char * b) {

    bool overflow = 0;
    int x = 0;

    for (; x < 25; x++) {
        a[x] += b[x] + overflow;
        overflow = (a[x] < (b[x] + overflow)) ? 1 : 0;
    }

    // Propagate overflow up the whole length of a if necessary

    while (overflow && x < 25)
        overflow = ++a[x++] == 0; 

}

bool Base58AddrToHash(const char * addr, int addrlen, unsigned char * hash, int * prefix) {

    unsigned char fullData[25];
    unsigned char addPart[25];

    memset(fullData, 0, 25);

    // Work backwards though string

    for (int x = addrlen; x-- != 0;) { 

        // Get index in alphabet array

        char c = addr[x];

        if (c != '1') { 

            char * loc = strchr(BASE_58_CHARS, c);
            if (loc == NULL)
                return false;

            memcpy(addPart, base58Powers[addrlen - 1 - x], 25);
            AddrDataMultByUInt8(addPart, loc - BASE_58_CHARS);
            AddrDataAdd(fullData, addPart);

        }
    }

    // Verify Zeros

    int zeros = 0;

    for (int x = 0; x < addrlen; x++)
        if (addr[x] == '1')
            zeros++;
        else
            break;

    int actualZeros = 0;
    for(int x = 25; x-- != 0 && fullData[x] == 0; actualZeros++);

    if (zeros != actualZeros)
        return false;

    // Reverse data

    unsigned char reversed[25];
    for (int x = 0; x < 25; x++)
        reversed[24 - x] = fullData[x];

    // Verify checksum

    unsigned char checksumhash[32];
    Sha256d(reversed, 21, checksumhash);

    if (memcmp(checksumhash, reversed + 21, 4) != 0)
        return false;

    // Return address data

    memcpy(hash, reversed + 1, 20);
    *prefix = reversed[0];

    return true;

}

