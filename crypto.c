// 
//  crypto.c
//  pybitcoin-crypto
// 
//  Copyright 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of unsigned charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#include "crypto.h"

// Load the crypto library

EC_GROUP * CRYPTO_CURVE;
pthread_key_t bnCtxKey;

static pthread_mutex_t * lock_cs;
static long * lock_count;

static void DestroyBnCtx(void * bnCtx) {
	BN_CTX_free(bnCtx);
}

unsigned long SetIdCallback(void) {
	return pthread_self();
}

void SetLockingCallback(int mode, int type, const char *file, int line) {

    UNUSED(file && line);

	(mode & CRYPTO_LOCK ? pthread_mutex_lock : pthread_mutex_unlock)(&(lock_cs[type]));

}

bool LoadCrypto(bool debug) {

	CRYPTO_CURVE = EC_GROUP_new_by_curve_name(NID_secp256k1);

	if (pthread_key_create(&bnCtxKey, DestroyBnCtx))
		return false;

	// Seed random from /dev/random unless debugging in which case do not seed
	
	if (!debug) {

		puts("pybitcoin_crypto: Gathering 32 bytes of entropy which may take some time");
		
		if (RAND_load_file("/dev/random", 32) != 32)
			return false;

		puts("pybitcoin_crypto: Entropy gathered!");

	}

	// Make thread safe

	int numLocks = CRYPTO_num_locks();
	
	lock_cs = OPENSSL_malloc(numLocks * sizeof(pthread_mutex_t));
	lock_count = OPENSSL_malloc(numLocks * sizeof(long));

	for (int x = 0; x < numLocks; x++) {

		lock_count[x] = 0;
		pthread_mutex_init(&(lock_cs[x]), NULL);
		
	}

	CRYPTO_set_id_callback(SetIdCallback);
	CRYPTO_set_locking_callback(SetLockingCallback);

	return true;

}

// Store one BN_CTX per thread that uses it

static BN_CTX * GetThreadBnCtx(void) {

	BN_CTX * ctx = pthread_getspecific(bnCtxKey); 

	if (ctx == NULL) {
		ctx = BN_CTX_new();
		if (pthread_setspecific(bnCtxKey, ctx)) {
			BN_CTX_free(ctx);
			ctx = NULL;
		}
	}

	return ctx;

}

// EC Key Functions

bool IsPubkeyValid(unsigned char * pubkey, int pubkeylen) {

	EC_POINT * pub = EC_POINT_new(CRYPTO_CURVE);
	bool res = (bool) EC_POINT_oct2point(CRYPTO_CURVE, pub, pubkey, pubkeylen, GetThreadBnCtx());
	EC_POINT_free(pub);

	return res;

}

void AddPoints(unsigned char * point1, unsigned char * point2) {
	
	// Get group
	EC_GROUP * group = EC_GROUP_new_by_curve_name(NID_secp256k1);

	// Get OpenSSL representations of points
	EC_POINT * p1 = EC_POINT_new(group);
	EC_POINT * p2 = EC_POINT_new(group);
	BN_CTX * ctx = BN_CTX_new();
	EC_POINT_oct2point(group, p1, point1, PUBKEY_SIZE, ctx);
	EC_POINT_oct2point(group, p2, point2, PUBKEY_SIZE, ctx);

	// Add points together
	EC_POINT * result = EC_POINT_new(group);
	EC_POINT_add(group, result, p1, p2, ctx);

	// Free points
	EC_POINT_free(p1);
	EC_POINT_free(p2);

	// Write result to point1
	EC_POINT_point2oct(group, result, POINT_CONVERSION_COMPRESSED, point1, PUBKEY_SIZE, ctx);

	// Free result, group and ctx
	EC_POINT_free(result);
	EC_GROUP_free(group);
	BN_CTX_free(ctx);
	
}

void KeyGetPublicKey(unsigned char * privKey, unsigned char * pubKey) {

	BIGNUM * privBn = BN_bin2bn(privKey, PRIVKEY_SIZE, NULL);
	EC_GROUP * group = EC_GROUP_new_by_curve_name(NID_secp256k1);
	EC_POINT * point = EC_POINT_new(group);
	BN_CTX * ctx = BN_CTX_new();

	EC_POINT_mul(group, point, privBn, NULL, NULL, ctx);
	EC_POINT_point2oct(group, point, POINT_CONVERSION_COMPRESSED, pubKey, PUBKEY_SIZE, ctx);

	BN_CTX_free(ctx);
	EC_POINT_free(point);
	EC_GROUP_free(group);
	BN_free(privBn);

}


// Random functions

bool SetRandomBytes(unsigned char * bytes, int num) {

	// Add to random seed for additional security
	
	struct timespec times;
	unsigned char nsbytes[8];

	clock_gettime(CLOCK_MONOTONIC, &times);	
	Int32ToArrayBigEndian(nsbytes, 0, times.tv_sec);
	Int32ToArrayBigEndian(nsbytes, 4, times.tv_nsec);
	
	RAND_add(nsbytes, 8, 1); 

	// Get random bytes
	return RAND_bytes(bytes, num) == 1;

}

// Hash functions

void GetAddressHash(unsigned char * input, int len, unsigned char * output) {
    
    unsigned char hash[32];
    SHA256(input, len, hash);
    RIPEMD160(hash, 32, output);

}

void Sha256d(unsigned char * input, int len, unsigned char * output) {

    unsigned char hash[32];
    SHA256(input, len, hash);
    SHA256(hash, 32, output);

}

