// 
//  hdwallet.h
//  pybitcoin-crypto
// 
//  Copyright 2013, 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of unsigned charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#ifndef HDKEYSH
#define HDKEYSH

//  Includes

#include "common.h"
#include "crypto.h"

// Macros

#define HD_KEY_STR_SIZE 82

// Enums

typedef enum{
	HD_KEY_VERSION_PROD_PRIVATE = 0x0488ADE4,
	HD_KEY_VERSION_PROD_PUBLIC = 0x0488B21E,
	HD_KEY_VERSION_TEST_PRIVATE = 0x04358394,
	HD_KEY_VERSION_TEST_PUBLIC = 0x043587CF,
} HDKeyVersion;

typedef enum{
	HD_KEY_TYPE_PRIVATE,
	HD_KEY_TYPE_PUBLIC,
	HD_KEY_TYPE_UNKNOWN
} HDKeyType;

typedef enum{
	HD_KEY_OFFSET_VERSION = 0,
	HD_KEY_OFFSET_DEPTH = 4,
	HD_KEY_OFFSET_FINGERPRINT = 5,
	HD_KEY_OFFSET_CHILD = 9,
	HD_KEY_OFFSET_CHAIN_CODE = 13,
	HD_KEY_OFFSET_KEY_DATA = 45
} HDKeyOffsets;

typedef enum{
	NETWORK_PRODUCTION,
	NETWORK_TEST,
	NETWORK_UNKNOWN
} Network;

// Structures

typedef struct{
	unsigned int priv:1;
	unsigned int childNumber:31;
} HDKeyChildID;

typedef struct{
	unsigned char key[PUBKEY_SIZE];
	unsigned char hash[20];
	bool hashSet;
} PubKeyInfo;

typedef struct{
	PubKeyInfo pubkey;
	unsigned char privkey[PRIVKEY_SIZE];
} KeyPair;

typedef struct{
	uint32_t versionBytes;
	HDKeyChildID childID;
	unsigned char depth;
	unsigned char chainCode[32];
	unsigned char parentFingerprint[4];
	KeyPair keyPair;
} HDKey;

// Constructors

HDKey * NewHDKey(void);
HDKey * NewHDKeyFromData(unsigned char * data);
KeyPair * NewKeyPair(bool priv);

// Initialisers

void InitHDKey(HDKey * key);
void InitRootHDKey(HDKey * key, HDKeyVersion version); 
void InitHDKeyWithPubKey(HDKey * key, unsigned char * pubkey, unsigned char * chainCode, bool production);
void InitHDKeyWithPrivKey(HDKey * key, unsigned char * privkey, unsigned char * chainCode, bool production);
bool InitHDKeyFromData(HDKey * key, unsigned char * data);
void InitKeyPair(KeyPair * key);

//  Functions

bool HDKeyDeriveChild(HDKey * parentKey, HDKeyChildID childID, HDKey * childKey);
bool HDKeyGenerateMaster(HDKey * key, bool production);
uint32_t HDKeyGetChildNumber(HDKeyChildID childID);
unsigned char * HDKeyGetHash(HDKey * key);
Network HDKeyGetNetwork(HDKeyVersion versionBytes);
unsigned char * HDKeyGetPrivateKey(HDKey * key);
unsigned char * HDKeyGetPublicKey(HDKey * key);
HDKeyType HDKeyGetType(HDKeyVersion versionBytes);
void HDKeyHmacSha512(unsigned char * inputData, unsigned char * chainCode, unsigned char * output);
void HDKeySerialise(HDKey * key, unsigned char * data);
bool KeyPairGenerate(KeyPair * keyPair);
unsigned char * KeyPairGetHash(KeyPair * key);

#endif


