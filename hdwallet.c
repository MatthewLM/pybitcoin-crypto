// 
//  hdwallet.c
//  pybitcoin-crypto
// 
//  Copyright 2013, 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of unsigned charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#include "hdwallet.h"

unsigned char CURVE_ORDER[32] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xBA,0xAE,0xDC,0xE6,0xAF,0x48,0xA0,0x3B,0xBF,0xD2,0x5E,0x8C,0xD0,0x36,0x41,0x41};

HDKey * NewHDKey(void) {

	HDKey * key = malloc(sizeof(*key));
	InitHDKey(key);

	return key;

}

HDKey * NewHDKeyFromData(unsigned char * data) {

	HDKey * key = NewHDKey();
	return InitHDKeyFromData(key, data) ? key : (free(key), NULL);

}

KeyPair * NewKeyPair(bool priv) {

	KeyPair * key = malloc(priv ? sizeof(KeyPair) : sizeof(PubKeyInfo));
	InitKeyPair(key);

	return key;

}

void InitHDKey(HDKey * key) {

	InitKeyPair(&key->keyPair);

}

void InitRootHDKey(HDKey * key, HDKeyVersion version) {

	key->versionBytes = version;
    // It could be private but the BIP specifies 0 for the child number.
	key->childID.priv = false; 
	key->childID.childNumber = 0;
	key->depth = 0;
	memset(key->parentFingerprint, 0, 4);

}

void InitHDKeyWithPubKey(HDKey * key, unsigned char * pubkey, unsigned char * chainCode, bool production) {

    InitHDKey(key);
    InitRootHDKey(key, production ? HD_KEY_VERSION_PROD_PUBLIC : HD_KEY_VERSION_TEST_PUBLIC);

    memcpy(key->keyPair.pubkey.key, pubkey, PUBKEY_SIZE);
    memcpy(key->chainCode, chainCode, 32);

}

void InitHDKeyWithPrivKey(HDKey * key, unsigned char * privkey, unsigned char * chainCode, bool production) {

    InitHDKey(key);
    InitRootHDKey(key, production ? HD_KEY_VERSION_PROD_PRIVATE : HD_KEY_VERSION_TEST_PRIVATE);

    memcpy(key->keyPair.privkey, privkey, PRIVKEY_SIZE);
    memcpy(key->chainCode, chainCode, 32);

    // Also calculate public key
	KeyGetPublicKey(key->keyPair.privkey, key->keyPair.pubkey.key);

}

bool InitHDKeyFromData(HDKey * key, unsigned char * data) {

	InitHDKey(key);

	// Set version bytes
    key->versionBytes = ArrayToInt32BigEndian(data, HD_KEY_OFFSET_VERSION);

	// Chain code
	memcpy(key->chainCode, data + HD_KEY_OFFSET_CHAIN_CODE, 32);

	if (key->versionBytes == HD_KEY_VERSION_PROD_PRIVATE
        || key->versionBytes == HD_KEY_VERSION_TEST_PRIVATE) {

		// Private
		memcpy(key->keyPair.privkey, data + HD_KEY_OFFSET_KEY_DATA + 1, 32);

		// Calculate public key
		KeyGetPublicKey(HDKeyGetPrivateKey(key), HDKeyGetPublicKey(key));

	}else
		// Assume public
		memcpy(HDKeyGetPublicKey(key), data + HD_KEY_OFFSET_KEY_DATA, 33);

	// Depth
	key->depth = data[HD_KEY_OFFSET_DEPTH];

	if (key->depth == 0) {

		// Master
		key->childID.priv = false;
		key->childID.childNumber = 0;
		if (memcmp(data + HD_KEY_OFFSET_FINGERPRINT, (unsigned char [4]){0,0,0,0}, 4) != 0)
			return false;

	}else{

		// Not master
		uint32_t childNum = ArrayToInt32BigEndian(data, HD_KEY_OFFSET_CHILD);
		key->childID.priv = childNum >> 31;
		key->childID.childNumber = childNum & 0x7fffffff;

	}

	return true;

}

void InitKeyPair(KeyPair * key) {

	key->pubkey.hashSet = false;

}

bool HDKeyDeriveChild(HDKey * parentKey, HDKeyChildID childID, HDKey * childKey) {

	unsigned char hash[64], inputData[37];
	HDKeyType type = HDKeyGetType(parentKey->versionBytes);

	// Add childID to inputData
	Int32ToArrayBigEndian(inputData, 33, HDKeyGetChildNumber(childID));

	if (childID.priv) {

		// Private key derivation

		// Parent must be private
		if (type != HD_KEY_TYPE_PRIVATE) 
			return false;

		// Add private key
		inputData[0] = 0;
		memcpy(inputData + 1, HDKeyGetPrivateKey(parentKey), 32);

	}else{

		// Public key derivation

		// Can be public or private
		if (type == HD_KEY_TYPE_UNKNOWN) 
			return false;

		// Add the public key
		memcpy(inputData, HDKeyGetPublicKey(parentKey), 33);

	}

	// Get HMAC-SHA512 hash
	HDKeyHmacSha512(inputData, parentKey->chainCode, hash);

	// Copy over chain code, childID and version bytes
	memcpy(childKey->chainCode, hash + 32, 32);
	childKey->childID = childID;
	childKey->versionBytes = parentKey->versionBytes;

	// Set fingerprint of parent
	memcpy(childKey->parentFingerprint, HDKeyGetHash(parentKey), 4);

	// Set depth
	childKey->depth = parentKey->depth + 1;

	// Calculate key
	if (type == HD_KEY_TYPE_PRIVATE) {

		// Calculating the private key
		// Add private key to first 32 bytes and modulo the order the curve
		// Split into four 64bit integers and add each one
		
		bool overflow = 0;

		for (unsigned char x = 4; x--;) {
			uint64_t a = ArrayToInt64BigEndian(hash, 8*x);
			uint64_t b = ArrayToInt64BigEndian(HDKeyGetPrivateKey(parentKey), 8*x) + overflow;
			uint64_t c = a + b;
			overflow = (c < b)? 1 : 0;
			Int64ToArrayBigEndian(HDKeyGetPrivateKey(childKey), 8*x, c);
		}

		if (overflow || memcmp(HDKeyGetPrivateKey(childKey), CURVE_ORDER, 32) > 0) {

			// Take away CURVE_ORDER
			// Overflow can be ignored. This would disappear by the carry
			
			bool carry = 0;

			for (unsigned char x = 4; x--;) {
				uint64_t a = ArrayToInt64BigEndian(HDKeyGetPrivateKey(childKey), 8*x);
				uint64_t b = ArrayToInt64BigEndian(CURVE_ORDER, 8*x) + carry;
				carry = a < b;
				a -= b;
				Int64ToArrayBigEndian(HDKeyGetPrivateKey(childKey), 8*x, a);
			}

		}

		// Derive public key from private key
		KeyGetPublicKey(childKey->keyPair.privkey, HDKeyGetPublicKey(childKey));

	}else{

		// Calculating the public key
		// Multiply the first 32 bytes by the base point (derive public key) and add to the public key point.
		KeyGetPublicKey(hash, HDKeyGetPublicKey(childKey));
		AddPoints(HDKeyGetPublicKey(childKey), HDKeyGetPublicKey(parentKey));

	}

	return true;

}

bool HDKeyGenerateMaster(HDKey * key, bool production) {

    InitRootHDKey(key, production ? HD_KEY_VERSION_PROD_PRIVATE : HD_KEY_VERSION_TEST_PRIVATE);

	if (!KeyPairGenerate(&key->keyPair)) 
		return false;

	// Make chain code by hashing private key
	SHA256(HDKeyGetPrivateKey(key), 32, key->chainCode);

	return true;

}

uint32_t HDKeyGetChildNumber(HDKeyChildID childID) {

	return (childID.priv << 31) | childID.childNumber;

}

unsigned char * HDKeyGetHash(HDKey * key) {

	return KeyPairGetHash(&key->keyPair);

}

Network HDKeyGetNetwork(HDKeyVersion versionBytes) {

	if (versionBytes == HD_KEY_VERSION_PROD_PUBLIC
		|| versionBytes == HD_KEY_VERSION_PROD_PRIVATE)
		return NETWORK_PRODUCTION;

	if (versionBytes == HD_KEY_VERSION_TEST_PUBLIC
		|| versionBytes == HD_KEY_VERSION_TEST_PRIVATE)
		return NETWORK_TEST;

	return NETWORK_UNKNOWN;

}

unsigned char * HDKeyGetPrivateKey(HDKey * key) {

	return key->keyPair.privkey;

}

unsigned char * HDKeyGetPublicKey(HDKey * key) {

	return key->keyPair.pubkey.key;

}

HDKeyType HDKeyGetType(HDKeyVersion versionBytes) {

	if (versionBytes == HD_KEY_VERSION_PROD_PUBLIC
		|| versionBytes == HD_KEY_VERSION_TEST_PUBLIC)
		return HD_KEY_TYPE_PUBLIC;

	if (versionBytes == HD_KEY_VERSION_PROD_PRIVATE
		|| versionBytes == HD_KEY_VERSION_TEST_PRIVATE)
		return HD_KEY_TYPE_PRIVATE;

	return HD_KEY_TYPE_UNKNOWN;

}

void HDKeyHmacSha512(unsigned char * inputData, unsigned char * chainCode, unsigned char * output) {

	// SHA512 has block size of 1024 bits or 128 bytes
	unsigned char msg[192], inner[32], outer[32], hash[64]; 	

	memcpy(msg, chainCode, 32);

	for (unsigned char x = 0; x < 32; x++) {
		inner[x] = msg[x] ^ 0x36;
		outer[x] = msg[x] ^ 0x5c;
	}

	memcpy(msg, inner, 32);
	memset(msg + 32, 0x36, 96);
	memcpy(msg + 128, inputData, 37);

	SHA512(msg, 165, hash);

	memcpy(msg + 128, hash, 64);
	memcpy(msg, outer, 32);
	memset(msg + 32, 0x5c, 96);

	SHA512(msg, 192, output);

}

void HDKeySerialise(HDKey * key, unsigned char * data) {

	Int32ToArrayBigEndian(data, HD_KEY_OFFSET_VERSION, key->versionBytes);

	if (key->depth == 0)
		// Master
		memset(data + HD_KEY_OFFSET_DEPTH, 0, 9);
	else {
		// Not master
		data[HD_KEY_OFFSET_DEPTH] = key->depth;
		memcpy(data + HD_KEY_OFFSET_FINGERPRINT, key->parentFingerprint, 4);
		// Child number
		Int32ToArrayBigEndian(data, HD_KEY_OFFSET_CHILD, HDKeyGetChildNumber(key->childID));
	}

	// Chain code
	memcpy(data + HD_KEY_OFFSET_CHAIN_CODE, key->chainCode, 32);

	if (HDKeyGetType(key->versionBytes) == HD_KEY_TYPE_PRIVATE) {
		// Private
		data[HD_KEY_OFFSET_KEY_DATA] = 0;
		memcpy(data + HD_KEY_OFFSET_KEY_DATA + 1, HDKeyGetPrivateKey(key), 32);
	}else
		// Assume public
		memcpy(data + HD_KEY_OFFSET_KEY_DATA, HDKeyGetPublicKey(key), 33);

}

bool KeyPairGenerate(KeyPair * keyPair) {

	// Generate private key from a CS-PRNG.
	if (!SetRandomBytes(keyPair->privkey, 32)) 
		return false;

	// Get public key
	KeyGetPublicKey(keyPair->privkey, keyPair->pubkey.key);

	return true;

}

unsigned char * KeyPairGetHash(KeyPair * key) {

	if (!key->pubkey.hashSet) {
		unsigned char hash[32];
		SHA256(key->pubkey.key, PUBKEY_SIZE, hash);
		RIPEMD160(hash, 32, key->pubkey.hash);
	}

	return key->pubkey.hash;

}

