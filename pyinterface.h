// 
//  pyinterface.h
//  pybitcoin-crypto
// 
//  Copyright 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of unsigned charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#ifndef PYINTERFACEH
#define PYINTERFACEH

// Includes

#include <Python.h> // Python must come first
#include <structmember.h>
#include "crypto.h"
#include "hdwallet.h"
#include "pyfunc.h"
#include "address.h"
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

// Structures

typedef struct {
    PyObject_HEAD
    HDKey key;
} PyHDWallet_Object;

static int PyHDWallet_init(PyHDWallet_Object * self, PyObject *args, PyObject *kwds);
static PyObject * PyHDWallet_GetPrivate(PyHDWallet_Object * self);
static PyObject * PyHDWallet_GetPublic(PyHDWallet_Object * self);
static PyObject * PyHDWallet_GetChainCode(PyHDWallet_Object * self);

static PyMethodDef PyHDWallet_Methods[] = {
    {"get_private", (PyCFunction)PyHDWallet_GetPrivate, METH_NOARGS, "Get the private key as a byte array"},
    {"get_public", (PyCFunction)PyHDWallet_GetPublic, METH_NOARGS, "Get the public key as a byte array"},
    {"get_chain_code", (PyCFunction)PyHDWallet_GetChainCode, METH_NOARGS, "Get the chain code as a byte array"},
    {NULL}  
};

static PyTypeObject PyHDWallet_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "pybitcoin_crypto.HDWallet",
    sizeof(PyHDWallet_Object),
    0,                         /* tp_itemsize */
    0,                         /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,
    "Contains a master private key, public ket and chain code",
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    PyHDWallet_Methods,        /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)PyHDWallet_init, /* tp_init */
    0,                         /* tp_alloc */
    PyType_GenericNew,         /* tp_new */};

#endif

