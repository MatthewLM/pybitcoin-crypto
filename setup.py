# 
#  setup.py
#  pybitcoin-crypto
# 
#  Copyright 2014 Matthew Mitchell
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#  
#      The above copyright notice and this permission notice shall be included in
#      all copies or substantial portions of the Software.
#  
#      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#      THE SOFTWARE.

from distutils.core import setup, Extension
import subprocess
import os

def test_cflag(f):
    p = subprocess.Popen(["gcc", "-E", "-", f], stdin=subprocess.PIPE)
    p.communicate("")
    return p.returncode == 0

cflags = ['-std=gnu99', '-O0', '-g', '-Werror', '-Wall', '-Wextra', '-Wno-missing-field-initializers']
ldflags = ['-lrt',  '-L/usr/lib/x86_64-linux-gnu/']

module = Extension(
    'pybitcoin_crypto',
    library_dirs = ['/usr/lib/x86_64-linux-gnu'],
    include_dirs = ['/usr/include/openssl'],
    libraries = ['crypto'],
    sources = ['crypto.c', 'pyinterface.c', 'hdwallet.c', 'pyfunc.c', 'address.c'],
    extra_compile_args = cflags,
    extra_link_args = ldflags,
)

setup (
    name = 'pybitcoin_crypto',
    version = '1.0',
    description = 'HD wallets and EC key library for cryptocurrency use',
    author = 'Matthew Mitchell',
    author_email = 'matthewmitchell@thelibertyportal.com',
    ext_modules = [module]
)

