// 
//  common.h
//  pybitcoin-crypto
// 
//  Copyright 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifndef COMMONH
#define COMMONH

#define PUBKEY_SIZE 33
#define PRIVKEY_SIZE 32
#define MAX_ADDR_LEN 36
#define MAX_WIF_LEN 53

typedef enum {
    SCRIPT_OP_1 = 0x51,
    SCRIPT_OP_CHECKMULTISIG = 0xae,
} ScriptOpCodes;

#define ArrayToInt32BigEndian(arr, offset) ( \
	(arr)[offset] << 24 \
      | (uint32_t)(arr)[offset + 1] << 16 \
      | (uint32_t)(arr)[offset + 2] << 8 \
      | (uint32_t)(arr)[offset + 3] \
)

#define ArrayToInt64BigEndian(arr, offset) ( \
	ArrayToInt32BigEndian(arr, offset + 4) \
	| (uint64_t)(arr)[offset] << 56 \
	| (uint64_t)(arr)[offset + 1] << 48 \
	| (uint64_t)(arr)[offset + 2] << 40 \
	| (uint64_t)(arr)[offset + 3] << 32 \
)

#define Int32ToArrayBigEndian(arr, offset, i) \
	(arr)[offset] = (i) >> 24; \
	(arr)[offset + 1] = (i) >> 16; \
	(arr)[offset + 2] = (i) >> 8; \
	(arr)[offset + 3] = (i);

#define Int64ToArrayBigEndian(arr, offset, i) \
	Int32ToArrayBigEndian(arr, offset + 4, i) \
	(arr)[offset + 0] = (uint8_t)((i) >> 56); \
	(arr)[offset + 1] = (uint8_t)((i) >> 48); \
	(arr)[offset + 2] = (uint8_t)((i) >> 40); \
	(arr)[offset + 3] = (uint8_t)((i) >> 32);

#define UNUSED(x) (void)(x)

#endif
