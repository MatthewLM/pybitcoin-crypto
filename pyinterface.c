// 
//  pyinterface.c
//  pybitcoin-crypto
// 
//  Copyright 2014 Matthew Mitchell
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//  
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
//

// Python wrappers

#include "pyinterface.h"

// Module functions

static PyObject * PyLoadCrypto(PyObject * self, PyObject * args) {

    UNUSED(self);

    unsigned char debug;

    if (!PyArg_ParseTuple(args, "b", &debug))
        return NULL;

    if (!LoadCrypto(debug)) {
        PyErr_SetString(errno == ENOMEM ? PyExc_MemoryError : PyExc_OSError, "Could not load crypto library");
        return NULL;
    }

    Py_RETURN_NONE;

}

static PyObject * PyIsPubKeyValid(PyObject * self, PyObject * args) {

    UNUSED(self);

    PyObject * pubkeyObject;

    if (!PyArg_ParseTuple(args, "O!", &PyByteArray_Type, &pubkeyObject))
        return NULL;

    PyObject * result = PyBool_FromLong((long) IsPubkeyValid((unsigned char *)PyByteArray_AsString(pubkeyObject), (int)PyByteArray_Size(pubkeyObject)));

    return result;

}

static PyObject * PyGetRandomBytes(PyObject * self, PyObject * args) {

    UNUSED(self);

    int blen;

    if (!PyArg_ParseTuple(args, "i", &blen))
        return NULL;

    char bytes[blen];

    if (!SetRandomBytes((unsigned char *)bytes, blen)) {
        PyErr_SetString(PyExc_RuntimeError, "Could not get any random data");
        return NULL;
    }

    return PyByteArray_FromStringAndSize(bytes, blen);

}

static PyObject * PyGetTestPubkey(PyObject * self) {

    UNUSED(self);

    KeyPair key;
    InitKeyPair(&key);

    if (!KeyPairGenerate(&key)) {
        PyErr_SetString(PyExc_RuntimeError, "Could not generate key pair");
        return NULL;
    }

    return PyByteArray_FromStringAndSize((char *)key.pubkey.key, PUBKEY_SIZE);

}

bool PyDeriveKey(PyObject * keyIndices, HDKey * key) {

    Py_ssize_t numIndices = PyList_Size(keyIndices);

    if (numIndices == 0) {
        PyErr_SetString(PyExc_ValueError, "Empty index list");
        return false;
    }

    HDKey * parent = key;

    HDKey hdkey2;
    HDKey * child = &hdkey2;

    InitHDKey(child);

    HDKeyChildID childID = { false, 0 };

    // Get key we want

    for (int x = 0;; x++) {

        PyObject * idx = PyList_GetItem(keyIndices, x);
        if (! PyLong_CheckExact(idx)) {
            PyErr_Format(PyExc_TypeError, "Bad index at %i", x);
            return false;
        }

        long lidx = PyLong_AsLong(idx);
        if (lidx > INT32_MAX || lidx < 0) {
            PyErr_Format(PyExc_ValueError, "Index out of range at %i", x);
            return false;
        }

        childID.childNumber = lidx;

        HDKeyDeriveChild(parent, childID, child);

        // Swap child and parent and continue if not reached the end of the list

        if (x == numIndices - 1)
            break;

        HDKey * temp = parent;
        parent = child;
        child = temp;

    }

    // Set key
    memcpy(key, child, sizeof(*key));

    return true;

}

static PyObject * PyGetPublicKey(PyObject * self, PyObject * args) {

    UNUSED(self);

    PyObject * pubkeyObject;
    PyObject * chainCodeObject;
    PyObject * keyIndices;

    if (!PyArg_ParseTuple(args, "O!O!O!", 
                &PyByteArray_Type, &pubkeyObject,
                &PyByteArray_Type, &chainCodeObject,
                &PyList_Type, &keyIndices))
        return NULL;

    unsigned char * publickey;
    unsigned char * chaincode;

    if ((publickey = CheckAndGetBytes(pubkeyObject, PUBKEY_SIZE)) == NULL 
            || ! IsPubkeyValid(publickey, PUBKEY_SIZE)) {
        PyErr_SetString(PyExc_ValueError, "Bad public key");
        return NULL;
    }

    if ((chaincode = CheckAndGetBytes(chainCodeObject, 32)) == NULL) {
        PyErr_SetString(PyExc_ValueError, "Bad chaincode");
        return NULL;
    }

    HDKey key;

    InitHDKeyWithPubKey(&key, publickey, chaincode, true);
    if (! PyDeriveKey(keyIndices, &key))
        return NULL;

    // Now return child public key as python bytearray
    return PyByteArray_FromStringAndSize((char *)HDKeyGetPublicKey(&key), PUBKEY_SIZE);

}

static PyObject * PyEncodeWIF(PyObject * self, PyObject *args) {

    UNUSED(self);

    PyObject * privkeyObject;
    int prefix;

    if (!PyArg_ParseTuple(args, "O!i", 
                &PyByteArray_Type, &privkeyObject,
                &prefix))
        return NULL;

    unsigned char * privkey;

    if ((privkey = CheckAndGetBytes(privkeyObject, PRIVKEY_SIZE)) == NULL) {
        PyErr_SetString(PyExc_ValueError, "Bad private key");
        return NULL;
    }

    char addr[MAX_WIF_LEN];
    unsigned char wifdata[PRIVKEY_SIZE + 1];

    memcpy(wifdata, privkey, PRIVKEY_SIZE);
    wifdata[PRIVKEY_SIZE] = 1;

    DataToBase58Addr(wifdata, PRIVKEY_SIZE + 1, addr, prefix);

    return PyUnicode_FromStringAndSize(addr, strlen(addr));

}

static PyObject * PyGetWIF(PyObject * self, PyObject *args) {

    UNUSED(self);

    PyObject * privkeyObject;
    PyObject * chainCodeObject;
    PyObject * keyIndices;
    int prefix;

    if (!PyArg_ParseTuple(args, "O!O!O!i", 
                &PyByteArray_Type, &privkeyObject,
                &PyByteArray_Type, &chainCodeObject,
                &PyList_Type, &keyIndices,
                &prefix))
        return NULL;

    unsigned char * privkey;
    unsigned char * chaincode;

    if ((privkey = CheckAndGetBytes(privkeyObject, PRIVKEY_SIZE)) == NULL) {
        PyErr_SetString(PyExc_ValueError, "Bad private key");
        return NULL;
    }

    if ((chaincode = CheckAndGetBytes(chainCodeObject, 32)) == NULL) {
        PyErr_SetString(PyExc_ValueError, "Bad chaincode");
        return NULL;
    }

    HDKey key;

    InitHDKeyWithPrivKey(&key, privkey, chaincode, true);
    if (! PyDeriveKey(keyIndices, &key))
        return NULL;

    char addr[MAX_WIF_LEN];
    unsigned char wifdata[PRIVKEY_SIZE + 1];

    memcpy(wifdata, HDKeyGetPrivateKey(&key), PRIVKEY_SIZE);
    wifdata[PRIVKEY_SIZE] = 1;

    DataToBase58Addr(wifdata, PRIVKEY_SIZE + 1, addr, prefix);

    return PyUnicode_FromStringAndSize(addr, strlen(addr));

}

PyObject * PyGetMultisigAddr(PyObject * self, PyObject * args) {

    UNUSED(self);

    int m, n;
    PyObject * pubkeys;

    if (!PyArg_ParseTuple(args, "iO!", &m, &PyList_Type, &pubkeys))
        return NULL;

    n = (int)PyList_Size(pubkeys);
    if (n < 1 || n > 15) {
        PyErr_SetString(PyExc_ValueError, "List of public keys should be between 1 and 15 keys");
        return NULL;
    }

    // Construct script

    size_t scriptlen = 3 + n*(1 + PUBKEY_SIZE);
    unsigned char script[scriptlen];
    int cursor = 1;

    script[0] = SCRIPT_OP_1 + m - 1;

    for (int x = 0; x < n; x++, cursor += PUBKEY_SIZE) {

        script[cursor++] = PUBKEY_SIZE;

        PyObject * idx = PyList_GetItem(pubkeys, x);
        if (!PyByteArray_CheckExact(idx)) {
            PyErr_Format(PyExc_TypeError, "Pubkey at %i needs to be a bytearray", x);
            return NULL;
        }

        unsigned char * pk = CheckAndGetBytes(idx, PUBKEY_SIZE);
        if (pk == NULL) {
            PyErr_Format(PyExc_ValueError, "Pubkey at %i is not the correct size", x);
            return NULL;
        }

        // Copy over public key
        memcpy(script + cursor, pk, PUBKEY_SIZE);

    }

    script[cursor++] = SCRIPT_OP_1 + n - 1;
    script[cursor] = SCRIPT_OP_CHECKMULTISIG;

    // Script is done. Get and return address hash

    unsigned char addr[20];
    GetAddressHash(script, scriptlen, addr);
    return PyByteArray_FromStringAndSize((char *)addr, 20);

}

PyObject * PyGetBase58Addr(PyObject * self, PyObject * args) {

    UNUSED(self);

    PyObject * pyhash;
    int prefix;

    if (!PyArg_ParseTuple(args, "O!i", &PyByteArray_Type, &pyhash, &prefix))
        return NULL;

    unsigned char * hash = CheckAndGetBytes(pyhash, 20);
    if (hash == NULL) {
        PyErr_SetString(PyExc_ValueError, "Hash must be 20 bytes");
        return NULL;
    }

    // Now get base58 representation

    char addr[MAX_ADDR_LEN];
    DataToBase58Addr(hash, 20, addr, prefix);
    return PyUnicode_FromStringAndSize(addr, strlen(addr));

}

PyObject * PyCalcHash(PyObject * args, void (*hashFunc)(unsigned char *, int, unsigned char *), int outLen) {

    Py_buffer buf;

    if (!PyArg_ParseTuple(args, "y*", &buf))
        return NULL;

    // Get Sha256d hash and return it as bytearray

    unsigned char output[outLen];
    hashFunc(buf.buf, buf.len, output);    
    PyBuffer_Release(&buf);

    return PyByteArray_FromStringAndSize((char *)output, outLen);

}

PyObject * PySha256d(PyObject * self, PyObject * args) {

    UNUSED(self);
    return PyCalcHash(args, Sha256d, 32);

}

PyObject * PyHash160(PyObject * self, PyObject * args) {

    UNUSED(self);
    return PyCalcHash(args, GetAddressHash, 20);

}

PyObject * PyDecodeAddress(PyObject * self, PyObject * args) {

    UNUSED(self);

    const char * addr;
    int addrlen;

    if (!PyArg_ParseTuple(args, "s#", &addr, &addrlen))
        return NULL;

    unsigned char hash[20];
    int prefix;

    if (!Base58AddrToHash(addr, addrlen, hash, &prefix)) {
        PyErr_SetString(PyExc_ValueError, "Invalid address");
        return NULL;
    }

    PyObject * tuple = PyTuple_New(2);
    PyTuple_SET_ITEM(tuple, 0, PyLong_FromLong(prefix));
    PyTuple_SET_ITEM(tuple, 1, PyByteArray_FromStringAndSize((char *)hash, 20));

    return tuple;

}

// Wallet class

static int PyHDWallet_init(PyHDWallet_Object * self, PyObject *args, PyObject *kwds) {

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "", (char *[]){NULL}))
        return -1;

    InitHDKey(&self->key);
    HDKeyGenerateMaster(&self->key, true);

    return 0;

}

static PyObject * PyHDWallet_GetPrivate(PyHDWallet_Object * self) {
    return PyByteArray_FromStringAndSize((char *)self->key.keyPair.privkey, PRIVKEY_SIZE);
}

static PyObject * PyHDWallet_GetPublic(PyHDWallet_Object * self) {
    return PyByteArray_FromStringAndSize((char *)self->key.keyPair.pubkey.key, PUBKEY_SIZE);
}

static PyObject * PyHDWallet_GetChainCode(PyHDWallet_Object * self) {
    return PyByteArray_FromStringAndSize((char *)self->key.chainCode, 32);
}

// Method table and module definition

static PyMethodDef methods[] = {

    {"is_pubkey_valid",  (PyCFunction)PyIsPubKeyValid, METH_VARARGS, "Is an EC public key valid"},
    {"get_random_bytes",  (PyCFunction)PyGetRandomBytes, METH_VARARGS, "Set bytes with cryptographically secure random data"},
    {"load",  (PyCFunction)PyLoadCrypto, METH_VARARGS, "Load the library before use."},
    {"get_test_pubkey",  (PyCFunction)PyGetTestPubkey, METH_NOARGS, "Get a random public key suitable for testing"},
    {"get_public_key", (PyCFunction)PyGetPublicKey, METH_VARARGS, "From a list of public key derived indexes, master chain code and a master public key, return the key in the HD wallet"},
    {"get_wif", (PyCFunction)PyGetWIF, METH_VARARGS, "From a list of public key derived indexes, master chain code and a master private key, return a WIF string of the child private key."},
    {"encode_wif", (PyCFunction)PyEncodeWIF, METH_VARARGS, "Get's a WIF for the given private key and prefix."},
    {"get_multisig_address", (PyCFunction)PyGetMultisigAddr, METH_VARARGS, "Gets a multisignature P2SH address hash with mOfn keys from a list of keys"},
    {"base58addr", (PyCFunction)PyGetBase58Addr, METH_VARARGS, "Gets the base58 address representation of a 20 byte hash."},
    {"sha256d", (PyCFunction)PySha256d, METH_VARARGS, "Gets double SHA-256 hash"},
    {"hash160", (PyCFunction)PyHash160, METH_VARARGS, "Gets sha256 + ripemd-160 hash"},
    {"decodeaddress", (PyCFunction)PyDecodeAddress, METH_VARARGS, "Returns (prefix, hash) for address string"},
    {NULL, NULL, 0, NULL}

};

static struct PyModuleDef pybitcoin_crypto_module = {
    PyModuleDef_HEAD_INIT,
    "pybitcoin_crypto",
    NULL,
    -1,
    methods
};

// Initialisation of module

PyMODINIT_FUNC PyInit_pybitcoin_crypto(void) {

    if (PyType_Ready(&PyHDWallet_Type) < 0)
        return NULL;

    PyObject * m = PyModule_Create(&pybitcoin_crypto_module);
    if (m == NULL)
        return NULL;

    Py_INCREF(&PyHDWallet_Type);
    PyModule_AddObject(m, "HDWallet", (PyObject *)&PyHDWallet_Type);

    return m;

}

int main (int argc, char *argv[]) {

    UNUSED(argc);

    PyImport_AppendInittab("pybitcoin_crypto", PyInit_pybitcoin_crypto);

    size_t prognamesize = strlen(argv[0]) + 1;
    wchar_t progname[prognamesize];
    mbstowcs(progname, argv[0], prognamesize);
    Py_SetProgramName(progname);

    Py_Initialize();
    PyImport_ImportModule("pybitcoin_crypto");

    return 0;

}
